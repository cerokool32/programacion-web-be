package co.com.iue.programacionweb.controller.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PerfilDTO {

  private String nombre;
  private Date fechaNacimiento;
  private String correo;
  private String foto;
  private String pais;
  private String educacion;
  private String contextura;
  private String sexo;
  private String sexoInteres;
  private String contexturaInteres;
}
