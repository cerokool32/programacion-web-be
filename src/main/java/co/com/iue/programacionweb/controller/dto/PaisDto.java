package co.com.iue.programacionweb.controller.dto;

import lombok.Data;

@Data
public class PaisDto {

  private Integer id;

  private String pais;
}
