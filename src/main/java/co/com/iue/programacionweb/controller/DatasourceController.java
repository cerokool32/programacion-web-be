package co.com.iue.programacionweb.controller;

import co.com.iue.programacionweb.service.DatasourceService;
import co.com.iue.programacionweb.service.dto.KeyValueDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/datasource")
@CrossOrigin("*")
public class DatasourceController {

  @Autowired private DatasourceService datasourceService;

  @GetMapping("/pais")
  public List<KeyValueDTO> getPaises() {
    return datasourceService.getPaises();
  }

  @GetMapping("/pais/{paisId}/lugar")
  public List<KeyValueDTO> getLugares(@PathVariable("paisId") Integer paisId) {
    return datasourceService.getLugares(paisId);
  }

  @GetMapping("/sexo")
  public List<KeyValueDTO> getSexos() {
    return datasourceService.getSexos();
  }

  @GetMapping("/educacion")
  public List<KeyValueDTO> getEducaciones() {
    return datasourceService.getEducaciones();
  }

  @GetMapping("/contextura")
  public List<KeyValueDTO> getContexturas() {
    return datasourceService.getContexturas();
  }

  @GetMapping("/aficion")
  public List<KeyValueDTO> getAficiones() {
    return datasourceService.getAficiones();
  }
}
