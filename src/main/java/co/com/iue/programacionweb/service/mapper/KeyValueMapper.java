package co.com.iue.programacionweb.service.mapper;

import co.com.iue.programacionweb.jpa.model.Aficcion;
import co.com.iue.programacionweb.jpa.model.Contextura;
import co.com.iue.programacionweb.jpa.model.Educacion;
import co.com.iue.programacionweb.jpa.model.Lugar;
import co.com.iue.programacionweb.jpa.model.Pais;
import co.com.iue.programacionweb.jpa.model.Perfil;
import co.com.iue.programacionweb.jpa.model.PerfilAficcion;
import co.com.iue.programacionweb.jpa.model.PerfilLugar;
import co.com.iue.programacionweb.jpa.model.Sexo;
import co.com.iue.programacionweb.service.dto.KeyValueDTO;
import co.com.iue.programacionweb.service.dto.PerfilDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface KeyValueMapper {

  @Mapping(source = "id", target = "key")
  @Mapping(source = "pais", target = "value")
  KeyValueDTO entity2dto(Pais pais);

  @Mapping(source = "id", target = "key")
  @Mapping(source = "sexo", target = "value")
  KeyValueDTO entity2dto(Sexo sexo);

  @Mapping(source = "id", target = "key")
  @Mapping(source = "educacion", target = "value")
  KeyValueDTO entity2dto(Educacion educacion);

  @Mapping(source = "id", target = "key")
  @Mapping(source = "contextura", target = "value")
  KeyValueDTO entity2dto(Contextura contextura);

  @Mapping(source = "id", target = "key")
  @Mapping(source = "aficcion", target = "value")
  KeyValueDTO entity2dto(Aficcion aficcion);

  @Mapping(source = "id.aficcion.id", target = "key")
  @Mapping(source = "id.aficcion.aficcion", target = "value")
  KeyValueDTO entity2dto(PerfilAficcion perfilAficcion);

  @Mapping(source = "id", target = "key")
  @Mapping(source = "lugar", target = "value")
  KeyValueDTO entity2dto(Lugar lugar);

  @Mapping(source = "id.lugar.id", target = "key")
  @Mapping(source = "id.lugar.lugar", target = "value")
  KeyValueDTO entity2dto(PerfilLugar perfilLugar);

  @Mapping(source = "id", target = "key")
  @Mapping(source = "nombre", target = "value")
  KeyValueDTO entity2dto(Perfil perfil);

  @Mapping(source = "nombre", target = "nombre")
  @Mapping(source = "correo", target = "correo")
  @Mapping(source = "fechaNacimiento", target = "fechaNacimiento")
  @Mapping(source = "contextura.contextura", target = "contextura")
  @Mapping(source = "educacion.educacion", target = "educacion")
  @Mapping(source = "pais.pais", target = "pais")
  @Mapping(source = "sexo.sexo", target = "sexo")
  @Mapping(source = "contexturaInteres.contextura", target = "contexturaInteres")
  @Mapping(source = "sexoInteres.sexo", target = "sexoInteres")
  PerfilDTO entity2detailedDto(Perfil perfil);
}
