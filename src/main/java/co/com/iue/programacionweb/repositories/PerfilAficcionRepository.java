package co.com.iue.programacionweb.repositories;

import co.com.iue.programacionweb.jpa.model.Aficcion;
import co.com.iue.programacionweb.jpa.model.Perfil;
import co.com.iue.programacionweb.jpa.model.PerfilAficcion;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PerfilAficcionRepository
    extends JpaRepository<PerfilAficcion, Integer>, QuerydslPredicateExecutor<PerfilAficcion> {

  PerfilAficcion findById_PerfilAndId_Aficcion(Perfil perfil, Aficcion aficcion);

  List<PerfilAficcion> findById_Perfil(Perfil perfil);
}
