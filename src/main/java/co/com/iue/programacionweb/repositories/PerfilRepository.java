package co.com.iue.programacionweb.repositories;

import co.com.iue.programacionweb.jpa.model.Perfil;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PerfilRepository
    extends JpaRepository<Perfil, Integer>, QuerydslPredicateExecutor<Perfil> {

  Optional<Perfil> findByCorreoAndClave(String correo, String clave);
}
