package co.com.iue.programacionweb.repositories;

import co.com.iue.programacionweb.jpa.model.Pais;
import co.com.iue.programacionweb.jpa.model.Perfil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PaisRepository extends JpaRepository<Pais, Integer>, QuerydslPredicateExecutor<Pais> {

}
