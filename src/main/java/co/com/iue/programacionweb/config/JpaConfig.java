package co.com.iue.programacionweb.config;

import co.com.iue.programacionweb.ProgramacionWebApiApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories(basePackageClasses = ProgramacionWebApiApplication.class)
@EnableTransactionManagement
public class JpaConfig {

}
