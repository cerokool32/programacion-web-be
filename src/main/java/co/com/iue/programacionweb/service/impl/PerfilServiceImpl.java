package co.com.iue.programacionweb.service.impl;

import co.com.iue.programacionweb.jpa.model.Aficcion;
import co.com.iue.programacionweb.jpa.model.Lugar;
import co.com.iue.programacionweb.jpa.model.Perfil;
import co.com.iue.programacionweb.jpa.model.PerfilAficcion;
import co.com.iue.programacionweb.jpa.model.PerfilAficcionId;
import co.com.iue.programacionweb.jpa.model.PerfilLugar;
import co.com.iue.programacionweb.jpa.model.PerfilLugarId;
import co.com.iue.programacionweb.repositories.AficcionRepository;
import co.com.iue.programacionweb.repositories.ContexturaRepository;
import co.com.iue.programacionweb.repositories.EducacionRepository;
import co.com.iue.programacionweb.repositories.LugarRepository;
import co.com.iue.programacionweb.repositories.PaisRepository;
import co.com.iue.programacionweb.repositories.PerfilAficcionRepository;
import co.com.iue.programacionweb.repositories.PerfilLugarRepository;
import co.com.iue.programacionweb.repositories.PerfilRepository;
import co.com.iue.programacionweb.repositories.SexoRepository;
import co.com.iue.programacionweb.service.PerfilService;
import co.com.iue.programacionweb.service.dto.KeyValueDTO;
import co.com.iue.programacionweb.service.dto.PerfilDTO;
import co.com.iue.programacionweb.service.dto.RegisterDTO;
import co.com.iue.programacionweb.service.mapper.KeyValueMapper;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Slf4j
public class PerfilServiceImpl implements PerfilService {

  @Autowired private PerfilRepository perfilRepository;

  @Autowired private PaisRepository paisRepository;

  @Autowired private SexoRepository sexoRepository;

  @Autowired private EducacionRepository educacionRepository;

  @Autowired private ContexturaRepository contexturaRepository;

  @Autowired private AficcionRepository aficcionRepository;

  @Autowired private LugarRepository lugarRepository;

  @Autowired private PerfilAficcionRepository perfilAficcionRepository;

  @Autowired private PerfilLugarRepository perfilLugarRepository;

  @Autowired private KeyValueMapper keyValueMapper;

  @Override
  public Integer login(String correo, String password) {
    log.info("{} / {}", correo, password);
    Optional<Perfil> perfil = perfilRepository.findByCorreoAndClave(correo, password);
    if (perfil.isPresent()) {
      return perfil.get().getId();
    }
    return null;
  }

  @Override
  @Transactional
  public Integer createPerfil(RegisterDTO registerDTO) {
    Perfil perfil =
        Perfil.builder()
            .nombre(registerDTO.getNombre())
            .fechaNacimiento(registerDTO.getFechaNacimiento())
            .correo(registerDTO.getCorreo())
            .clave(registerDTO.getClave())
            .pais(paisRepository.getOne(registerDTO.getPais()))
            .sexo(sexoRepository.getOne(registerDTO.getSexo()))
            .educacion(educacionRepository.getOne(registerDTO.getEducacion()))
            .contextura(contexturaRepository.getOne(registerDTO.getContextura()))
            .sexoInteres(sexoRepository.getOne(registerDTO.getSexoInteres()))
            .contexturaInteres(contexturaRepository.getOne(registerDTO.getContexturaInteres()))
            .build();

    perfilRepository.save(perfil);

    return perfil.getId();
  }

  @Override
  @Transactional
  public void addPhotoToPerfil(Integer perfilId, MultipartFile multipartFile) throws Exception {
    Perfil perfil = perfilRepository.getOne(perfilId);
    perfil.setFoto(multipartFile.getBytes());
  }

  @Override
  @Transactional
  public void addAficion(Integer perfilId, Integer aficionId) {
    Perfil perfil = perfilRepository.getOne(perfilId);
    Aficcion aficcion = aficcionRepository.getOne(aficionId);

    PerfilAficcionId perfilAficcionId = new PerfilAficcionId();
    perfilAficcionId.setPerfil(perfil);
    perfilAficcionId.setAficcion(aficcion);

    PerfilAficcion perfilAficcion = new PerfilAficcion();
    perfilAficcion.setId(perfilAficcionId);

    perfilAficcionRepository.save(perfilAficcion);
  }

  @Override
  @Transactional
  public void addPhotoToAficion(Integer perfilId, Integer aficionId, MultipartFile multipartFile)
      throws Exception {
    Perfil perfil = perfilRepository.getOne(perfilId);
    Aficcion aficcion = aficcionRepository.getOne(aficionId);

    PerfilAficcion perfilAficcion =
        perfilAficcionRepository.findById_PerfilAndId_Aficcion(perfil, aficcion);
    perfilAficcion.setFoto(multipartFile.getBytes());
  }

  @Override
  public List<KeyValueDTO> getAficiones(Integer perfilId) {
    Perfil perfil = perfilRepository.getOne(perfilId);

    return perfilAficcionRepository.findById_Perfil(perfil).stream()
        .map(keyValueMapper::entity2dto)
        .collect(Collectors.toList());
  }

  @Override
  public byte[] getFotoAficion(Integer perfilId, Integer aficionId) {
    Perfil perfil = perfilRepository.getOne(perfilId);
    Aficcion aficcion = aficcionRepository.getOne(aficionId);

    PerfilAficcion perfilAficcion =
        perfilAficcionRepository.findById_PerfilAndId_Aficcion(perfil, aficcion);

    return perfilAficcion.getFoto();
  }

  @Override
  @Transactional
  public void addLugar(Integer perfilId, Integer lugarId, boolean conocido) {
    Perfil perfil = perfilRepository.getOne(perfilId);
    Lugar lugar = lugarRepository.getOne(lugarId);

    PerfilLugarId perfilLugarId = new PerfilLugarId();
    perfilLugarId.setPerfil(perfil);
    perfilLugarId.setLugar(lugar);

    PerfilLugar perfilLugar = new PerfilLugar();
    perfilLugar.setId(perfilLugarId);
    perfilLugar.setConocido(conocido);

    perfilLugarRepository.save(perfilLugar);
  }

  @Override
  @Transactional
  public void addPhotoToLugar(Integer perfilId, Integer lugarId, MultipartFile multipartFile)
      throws Exception {
    Perfil perfil = perfilRepository.getOne(perfilId);
    Lugar lugar = lugarRepository.getOne(lugarId);

    PerfilLugar perfilLugar = perfilLugarRepository.findById_PerfilAndId_Lugar(perfil, lugar);
    perfilLugar.setFoto(multipartFile.getBytes());
  }

  @Override
  public byte[] getFotoLugar(Integer perfilId, Integer lugarId) {
    Perfil perfil = perfilRepository.getOne(perfilId);
    Lugar lugar = lugarRepository.getOne(lugarId);

    PerfilLugar perfilLugar = perfilLugarRepository.findById_PerfilAndId_Lugar(perfil, lugar);

    return perfilLugar.getFoto();
  }

  @Override
  public List<KeyValueDTO> getLugares(Integer perfilId) {
    Perfil perfil = perfilRepository.getOne(perfilId);
    return perfilLugarRepository.findById_Perfil(perfil).stream()
        .map(keyValueMapper::entity2dto)
        .collect(Collectors.toList());
  }

  @Override
  public List<KeyValueDTO> getPerfiles() {
    return perfilRepository.findAll().stream()
        .map(keyValueMapper::entity2dto)
        .collect(Collectors.toList());
  }

  @Override
  public PerfilDTO getPerfil(Integer perfilId) {
    return keyValueMapper.entity2detailedDto(perfilRepository.getOne(perfilId));
  }
}
