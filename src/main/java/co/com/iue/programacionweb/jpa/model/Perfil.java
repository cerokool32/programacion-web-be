package co.com.iue.programacionweb.jpa.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Perfil")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Perfil {

  @Id
  @Column(name = "Id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "Nombre")
  private String nombre;

  @Column(name = "FechaNacimiento")
  private Date fechaNacimiento;

  @Column(name = "Correo")
  private String correo;

  @Column(name = "Clave")
  private String clave;

  @Lob
  @Column(name = "Foto", columnDefinition = "image")
  private byte[] foto;

  @ManyToOne
  @JoinColumn(name = "IdPais")
  private Pais pais;

  @ManyToOne
  @JoinColumn(name = "IdSexo")
  private Sexo sexo;

  @ManyToOne
  @JoinColumn(name = "IdEducacion")
  private Educacion educacion;

  @ManyToOne
  @JoinColumn(name = "IdContextura")
  private Contextura contextura;

  @ManyToOne
  @JoinColumn(name = "IdSexoInteres")
  private Sexo sexoInteres;

  @ManyToOne
  @JoinColumn(name = "IdContexturaInteres")
  private Contextura contexturaInteres;
}
