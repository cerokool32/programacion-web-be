package co.com.iue.programacionweb.service.impl;

import co.com.iue.programacionweb.jpa.model.Pais;
import co.com.iue.programacionweb.repositories.AficcionRepository;
import co.com.iue.programacionweb.repositories.ContexturaRepository;
import co.com.iue.programacionweb.repositories.EducacionRepository;
import co.com.iue.programacionweb.repositories.LugarRepository;
import co.com.iue.programacionweb.repositories.PaisRepository;
import co.com.iue.programacionweb.repositories.SexoRepository;
import co.com.iue.programacionweb.service.DatasourceService;
import co.com.iue.programacionweb.service.dto.KeyValueDTO;
import co.com.iue.programacionweb.service.mapper.KeyValueMapper;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DatasourceServiceImpl implements DatasourceService {

  @Autowired private PaisRepository paisRepository;

  @Autowired private SexoRepository sexoRepository;

  @Autowired private EducacionRepository educacionRepository;

  @Autowired private ContexturaRepository contexturaRepository;

  @Autowired private AficcionRepository aficcionRepository;

  @Autowired private LugarRepository lugarRepository;

  @Autowired private KeyValueMapper keyValueMapper;

  @Override
  public List<KeyValueDTO> getPaises() {
    return paisRepository.findAll().stream()
        .map(keyValueMapper::entity2dto)
        .collect(Collectors.toList());
  }

  @Override
  public List<KeyValueDTO> getSexos() {
    return sexoRepository.findAll().stream()
        .map(keyValueMapper::entity2dto)
        .collect(Collectors.toList());
  }

  @Override
  public List<KeyValueDTO> getEducaciones() {
    return educacionRepository.findAll().stream()
        .map(keyValueMapper::entity2dto)
        .collect(Collectors.toList());
  }

  @Override
  public List<KeyValueDTO> getContexturas() {
    return contexturaRepository.findAll().stream()
        .map(keyValueMapper::entity2dto)
        .collect(Collectors.toList());
  }

  @Override
  public List<KeyValueDTO> getAficiones() {
    return aficcionRepository.findAll().stream()
        .map(keyValueMapper::entity2dto)
        .collect(Collectors.toList());
  }

  @Override
  public List<KeyValueDTO> getLugares(Integer paisId) {
    Pais pais = paisRepository.getOne(paisId);
    return lugarRepository.findAllByPais(pais).stream()
        .map(keyValueMapper::entity2dto)
        .collect(Collectors.toList());
  }
}
