package co.com.iue.programacionweb.repositories;

import co.com.iue.programacionweb.jpa.model.Sexo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface SexoRepository extends JpaRepository<Sexo, Integer>, QuerydslPredicateExecutor<Sexo> {

}
