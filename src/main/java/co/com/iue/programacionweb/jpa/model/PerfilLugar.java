package co.com.iue.programacionweb.jpa.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "PerfilLugar")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PerfilLugar {

  @EmbeddedId private PerfilLugarId id;

  @Column(name = "Conocido")
  private Boolean conocido;

  @Lob
  @Column(name = "Foto", columnDefinition = "image")
  private byte[] foto;
}
