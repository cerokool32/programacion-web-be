package co.com.iue.programacionweb.service;

import co.com.iue.programacionweb.service.dto.KeyValueDTO;
import java.util.List;

public interface DatasourceService {

  List<KeyValueDTO> getPaises();

  List<KeyValueDTO> getLugares(Integer paisId);

  List<KeyValueDTO> getSexos();

  List<KeyValueDTO> getEducaciones();

  List<KeyValueDTO> getContexturas();

  List<KeyValueDTO> getAficiones();
}
