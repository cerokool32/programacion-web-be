package co.com.iue.programacionweb.jpa.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "PerfilAficcion")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PerfilAficcion {

  @EmbeddedId private PerfilAficcionId id;

  @Lob
  @Column(name = "Foto", columnDefinition = "image")
  private byte[] foto;
}
