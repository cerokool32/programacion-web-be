package co.com.iue.programacionweb.controller;

import co.com.iue.programacionweb.service.PerfilService;
import co.com.iue.programacionweb.service.dto.KeyValueDTO;
import co.com.iue.programacionweb.service.dto.LoginDTO;
import co.com.iue.programacionweb.service.dto.PerfilDTO;
import co.com.iue.programacionweb.service.dto.RegisterDTO;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/perfil")
@CrossOrigin("*")
@Slf4j
public class PerfilController {

  @Autowired private PerfilService perfilService;

  @PostMapping
  public Integer registrarPerfil(@RequestBody RegisterDTO registerDTO) {
    return perfilService.createPerfil(registerDTO);
  }

  @PostMapping("/foto/{perfilId}")
  public void registrarFotoPerfil(
      @PathVariable("perfilId") Integer perfilId, @RequestParam("file0") MultipartFile file)
      throws Exception {
    perfilService.addPhotoToPerfil(perfilId, file);
  }

  @PostMapping("/login")
  public Integer login(@RequestBody LoginDTO loginDTO) {
    return perfilService.login(loginDTO.getCorreo(), loginDTO.getClave());
  }

  @PostMapping("{perfildId}/aficion/{aficionId}")
  public void addAficion(
      @PathVariable("perfildId") Integer perfilId, @PathVariable("aficionId") Integer aficionId) {
    perfilService.addAficion(perfilId, aficionId);
  }

  @PostMapping("{perfilId}/aficion/{aficionId}/photo")
  public void addAficionPhoto(
      @PathVariable("perfilId") Integer perfilId,
      @PathVariable("aficionId") Integer aficionId,
      @RequestParam("file0") MultipartFile file)
      throws Exception {
    perfilService.addPhotoToAficion(perfilId, aficionId, file);
  }

  @GetMapping("{perfilId}/aficion")
  public List<KeyValueDTO> getAficionList(@PathVariable("perfilId") Integer perfilId) {
    return perfilService.getAficiones(perfilId);
  }

  @GetMapping("{perfilId}/aficion/{aficionId}/photo")
  public byte[] getAficionPhoto(
      @PathVariable("perfilId") Integer perfilId, @PathVariable("aficionId") Integer aficionId) {
    return perfilService.getFotoAficion(perfilId, aficionId);
  }

  @GetMapping("{perfilId}/lugar")
  public List<KeyValueDTO> getLugarList(@PathVariable("perfilId") Integer perfilId) {
    return perfilService.getLugares(perfilId);
  }

  @PostMapping("{perfilId}/lugar/{lugarId}")
  public void addLugar(
      @PathVariable("perfilId") Integer perfilId, @PathVariable("lugarId") Integer lugarId) {
    perfilService.addLugar(perfilId, lugarId, false);
  }

  @PostMapping("{perfilId}/lugar/{lugarId}/conocido")
  public void addLugarConocido(
      @PathVariable("perfilId") Integer perfilId, @PathVariable("lugarId") Integer lugarId) {
    perfilService.addLugar(perfilId, lugarId, true);
  }

  @PostMapping("{perfilId}/lugar/{lugarId}/photo")
  public void addLugarPhoto(
      @PathVariable("perfilId") Integer perfilId,
      @PathVariable("lugarId") Integer lugarId,
      @RequestParam("file0") MultipartFile file)
      throws Exception {
    perfilService.addPhotoToLugar(perfilId, lugarId, file);
  }

  @GetMapping("{perfilId}/lugar/{lugarId}/photo")
  public byte[] getLugarPhoto(
      @PathVariable("perfilId") Integer perfilId, @PathVariable("lugarId") Integer lugarId) {
    return perfilService.getFotoLugar(perfilId, lugarId);
  }

  @GetMapping("perfiles")
  public List<KeyValueDTO> getPerfiles() {
    return perfilService.getPerfiles();
  }

  @GetMapping("perfiles/{perfilId}")
  public PerfilDTO getPerfiles(@PathVariable("perfilId") Integer perfilId) {
    return perfilService.getPerfil(perfilId);
  }
}
