package co.com.iue.programacionweb.service;

import co.com.iue.programacionweb.service.dto.KeyValueDTO;
import co.com.iue.programacionweb.service.dto.PerfilDTO;
import co.com.iue.programacionweb.service.dto.RegisterDTO;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public interface PerfilService {

  Integer login(String correo, String password);

  Integer createPerfil(RegisterDTO registerDTO);

  void addPhotoToPerfil(Integer perfilId, MultipartFile multipartFile) throws Exception;

  void addAficion(Integer perfilId, Integer aficionId);

  void addPhotoToAficion(Integer perfilId, Integer aficionId, MultipartFile multipartFile)
      throws Exception;

  List<KeyValueDTO> getAficiones(Integer perfilId);

  byte[] getFotoAficion(Integer perfilId, Integer aficionId);

  List<KeyValueDTO> getLugares(Integer perfilId);

  void addLugar(Integer perfilId, Integer lugarId, boolean conocido);

  void addPhotoToLugar(Integer perfilId, Integer lugarId, MultipartFile multipartFile)
      throws Exception;

  byte[] getFotoLugar(Integer perfilId, Integer lugarId);

  List<KeyValueDTO> getPerfiles();

  PerfilDTO getPerfil(Integer perfilId);
}
