package co.com.iue.programacionweb.controller.dto;

import lombok.Data;

@Data
public class EducacionDto {

  private Integer id;

  private String educacion;
}
