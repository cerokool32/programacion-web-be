package co.com.iue.programacionweb.controller.dto;

import lombok.Data;

@Data
public class LugarDto {

  private Integer id;

  private Integer paisId;

  private String lugar;
}
