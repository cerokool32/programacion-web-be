package co.com.iue.programacionweb.jpa.model;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PerfilAficcionId implements Serializable {

  @ManyToOne
  @JoinColumn(name = "IdPerfil")
  private Perfil perfil;

  @ManyToOne
  @JoinColumn(name = "IdAficcion")
  private Aficcion aficcion;
}
