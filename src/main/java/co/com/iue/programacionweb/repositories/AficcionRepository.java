package co.com.iue.programacionweb.repositories;

import co.com.iue.programacionweb.jpa.model.Aficcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface AficcionRepository
    extends JpaRepository<Aficcion, Integer>, QuerydslPredicateExecutor<Aficcion> {}
