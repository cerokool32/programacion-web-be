package co.com.iue.programacionweb.repositories;

import co.com.iue.programacionweb.jpa.model.Educacion;
import co.com.iue.programacionweb.jpa.model.Lugar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface EducacionRepository extends JpaRepository<Educacion, Integer>, QuerydslPredicateExecutor<Educacion> {

}
