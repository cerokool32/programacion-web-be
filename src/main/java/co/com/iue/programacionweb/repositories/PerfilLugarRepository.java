package co.com.iue.programacionweb.repositories;

import co.com.iue.programacionweb.jpa.model.Lugar;
import co.com.iue.programacionweb.jpa.model.Perfil;
import co.com.iue.programacionweb.jpa.model.PerfilLugar;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PerfilLugarRepository
    extends JpaRepository<PerfilLugar, Integer>, QuerydslPredicateExecutor<PerfilLugar> {

  PerfilLugar findById_PerfilAndId_Lugar(Perfil perfil, Lugar lugar);

  List<PerfilLugar> findById_Perfil(Perfil perfil);
}
