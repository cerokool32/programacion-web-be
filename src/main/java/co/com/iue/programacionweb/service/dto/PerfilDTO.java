package co.com.iue.programacionweb.service.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PerfilDTO {

  private String nombre;
  private Date fechaNacimiento;
  private String correo;
  private String pais;
  private String educacion;
  private String contextura;
  private String sexo;
  private String sexoInteres;
  private String contexturaInteres;
}
