package co.com.iue.programacionweb.controller;

import co.com.iue.programacionweb.controller.dto.AficionDto;
import co.com.iue.programacionweb.controller.dto.ContexturaDto;
import co.com.iue.programacionweb.controller.dto.EducacionDto;
import co.com.iue.programacionweb.controller.dto.LugarDto;
import co.com.iue.programacionweb.controller.dto.PaisDto;
import co.com.iue.programacionweb.controller.dto.PerfilDTO;
import co.com.iue.programacionweb.controller.mapper.CrudMapper;
import co.com.iue.programacionweb.jpa.model.Aficcion;
import co.com.iue.programacionweb.jpa.model.Contextura;
import co.com.iue.programacionweb.jpa.model.Educacion;
import co.com.iue.programacionweb.jpa.model.Lugar;
import co.com.iue.programacionweb.jpa.model.Pais;
import co.com.iue.programacionweb.jpa.model.Perfil;
import co.com.iue.programacionweb.repositories.AficcionRepository;
import co.com.iue.programacionweb.repositories.ContexturaRepository;
import co.com.iue.programacionweb.repositories.EducacionRepository;
import co.com.iue.programacionweb.repositories.LugarRepository;
import co.com.iue.programacionweb.repositories.PaisRepository;
import co.com.iue.programacionweb.repositories.PerfilRepository;
import java.util.Base64;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/crud")
@CrossOrigin("*")
public class CrudController {

  @Autowired private AficcionRepository aficcionRepository;

  @Autowired private EducacionRepository educacionRepository;

  @Autowired private PaisRepository paisRepository;

  @Autowired private ContexturaRepository contexturaRepository;

  @Autowired private LugarRepository lugarRepository;

  @Autowired private PerfilRepository perfilRepository;

  @Autowired private CrudMapper crudMapper;

  // Perfil de usuario con foto en base 64
  @GetMapping("perfil")
  public PerfilDTO getPerfil(Integer perfilId) {
    Perfil perfil = perfilRepository.getOne(perfilId);
    return PerfilDTO.builder()
        .nombre(perfil.getNombre())
        .contextura(perfil.getContextura().getContextura())
        .contexturaInteres(perfil.getContexturaInteres().getContextura())
        .correo(perfil.getCorreo())
        .educacion(perfil.getEducacion().getEducacion())
        .fechaNacimiento(perfil.getFechaNacimiento())
        .foto(Base64.getEncoder().encodeToString(perfil.getFoto()))
        .pais(perfil.getPais().getPais())
        .sexo(perfil.getSexo().getSexo())
        .sexoInteres(perfil.getSexoInteres().getSexo())
        .build();
  }

  // CRUD para Aficion
  @PostMapping("aficion")
  @Transactional
  public Integer createAficion(@RequestParam("aficion") String aficion) {
    Aficcion aficcion = new Aficcion();
    aficcion.setAficcion(aficion);
    aficcionRepository.save(aficcion);
    return aficcion.getId();
  }

  @GetMapping("aficion/{aficionId}")
  public AficionDto readAficion(@PathVariable("aficionId") Integer aficionId) {
    return crudMapper.entity2dto(aficcionRepository.getOne(aficionId));
  }

  @PutMapping("aficion/{aficionId}")
  @Transactional
  public void updateAficion(
      @PathVariable("aficionId") Integer aficionId, @RequestParam("aficion") String aficion) {
    Aficcion aficcion = aficcionRepository.getOne(aficionId);
    aficcion.setAficcion(aficion);
  }

  @DeleteMapping("aficion/{aficionId}")
  public void deleteAficion(Integer aficionId) {
    Aficcion aficcion = aficcionRepository.getOne(aficionId);
    aficcionRepository.delete(aficcion);
  }

  // CRUD para Educacion
  @PostMapping("educacion")
  @Transactional
  public Integer createEducacion(@RequestParam("educacion") String educacion) {
    Educacion educacion1 = new Educacion();
    educacion1.setEducacion(educacion);
    educacionRepository.save(educacion1);
    return educacion1.getId();
  }

  @GetMapping("educacion/{educacionId}")
  public EducacionDto readEducacion(@PathVariable("educacionId") Integer educacionId) {
    return crudMapper.entity2dto(educacionRepository.getOne(educacionId));
  }

  @PutMapping("educacion/{educacionId}")
  @Transactional
  public void updateEducacion(
      @PathVariable("educacionId") Integer educacionId,
      @RequestParam("educacion") String educacion) {
    Educacion educacion1 = educacionRepository.getOne(educacionId);
    educacion1.setEducacion(educacion);
  }

  @DeleteMapping("educacion/{educacionId}")
  public void deleteEducacion(@PathVariable("educacionId") Integer educacionId) {
    Educacion educacion = educacionRepository.getOne(educacionId);
    educacionRepository.delete(educacion);
  }

  // CRUD para Pais
  @PostMapping("pais")
  @Transactional
  public Integer createPAis(@RequestParam("pais") String pais) {
    Pais pais1 = new Pais();
    pais1.setPais(pais);
    paisRepository.save(pais1);
    return pais1.getId();
  }

  @GetMapping("pais/{paisId}")
  public PaisDto readPais(@PathVariable("paisId") Integer paisId) {
    return crudMapper.entity2dto(paisRepository.getOne(paisId));
  }

  @PutMapping("pais/{paisId}")
  @Transactional
  public void updatePais(
      @PathVariable("paisId") Integer paisId, @RequestParam("pais") String pais) {
    Pais pais1 = paisRepository.getOne(paisId);
    pais1.setPais(pais);
  }

  @DeleteMapping("pais/{paisId}")
  public void deletePais(Integer paisId) {
    Pais pais = paisRepository.getOne(paisId);
    paisRepository.delete(pais);
  }

  // CRUD para Contextura
  @PostMapping("contextura")
  @Transactional
  public Integer createContextura(@RequestParam("contextura") String contextura) {
    Contextura contextura1 = new Contextura();
    contextura1.setContextura(contextura);
    contexturaRepository.save(contextura1);
    return contextura1.getId();
  }

  @GetMapping("contexura/{contexturaId}")
  public ContexturaDto readContextura(Integer contexturaId) {
    return crudMapper.entity2dto(contexturaRepository.getOne(contexturaId));
  }

  @PutMapping("contextura/{contexturaId}")
  @Transactional
  public void updateContextura(
      @PathVariable("contexturaId") Integer contexturaId,
      @RequestParam("contextura") String contextura) {
    Contextura contextura1 = contexturaRepository.getOne(contexturaId);
    contextura1.setContextura(contextura);
  }

  @DeleteMapping("contextura/{contexturaId}")
  public void deleteContextura(@PathVariable("contexturaId") Integer contexturaId) {
    Contextura contextura = contexturaRepository.getOne(contexturaId);
    contexturaRepository.delete(contextura);
  }

  // CRUD para Lugar
  @PostMapping("lugar")
  @Transactional
  public Integer createLugar(
      @RequestParam("lugar") String lugar, @RequestParam("paisId") Integer paisId) {
    Lugar lugar1 = new Lugar();
    lugar1.setLugar(lugar);
    lugar1.setPais(paisRepository.getOne(paisId));
    lugarRepository.save(lugar1);
    return lugar1.getId();
  }

  @GetMapping("lugar/{lugarId}")
  public LugarDto readLugar(@PathVariable("lugarId") Integer lugarId) {
    return crudMapper.entity2dto(lugarRepository.getOne(lugarId));
  }

  @PutMapping("lugar/{paisId}/{lugarId}")
  @Transactional
  public void updateLugar(
      @PathVariable("lugarId") Integer lugarId,
      @PathVariable("paisId") Integer paisId,
      @RequestParam("lugar") String lugar) {
    Lugar lugar1 = lugarRepository.getOne(lugarId);
    lugar1.setLugar(lugar);
    lugar1.setPais(paisRepository.getOne(paisId));
  }

  @DeleteMapping("lugar/{lugarId}")
  public void deleteLugar(@PathVariable("lugarId") Integer lugarId) {
    Lugar lugar = lugarRepository.getOne(lugarId);
    lugarRepository.delete(lugar);
  }
}
