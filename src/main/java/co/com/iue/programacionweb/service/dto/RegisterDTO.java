package co.com.iue.programacionweb.service.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDTO {

  private String nombre;
  private Date fechaNacimiento;
  private String correo;
  private String clave;
  private Integer pais;
  private Integer educacion;
  private Integer contextura;
  private Integer sexo;
  private Integer sexoInteres;
  private Integer contexturaInteres;
}
