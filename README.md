# Getting Started

# Running the project
The application uses [Spring Boot](http://projects.spring.io/spring-boot/), so it is easy to run. You can start it any of a few ways:
* Run the `main` method from `InChannelAllocationApiApplication`
* Use the Maven Spring Boot plugin: `mvn spring-boot:run`

# Viewing the running application
To view the running application, visit [http://localhost:8080](http://localhost:8080) in your browser
