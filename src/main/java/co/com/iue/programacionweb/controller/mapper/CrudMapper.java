package co.com.iue.programacionweb.controller.mapper;

import co.com.iue.programacionweb.controller.dto.AficionDto;
import co.com.iue.programacionweb.controller.dto.ContexturaDto;
import co.com.iue.programacionweb.controller.dto.EducacionDto;
import co.com.iue.programacionweb.controller.dto.LugarDto;
import co.com.iue.programacionweb.controller.dto.PaisDto;
import co.com.iue.programacionweb.jpa.model.Aficcion;
import co.com.iue.programacionweb.jpa.model.Contextura;
import co.com.iue.programacionweb.jpa.model.Educacion;
import co.com.iue.programacionweb.jpa.model.Lugar;
import co.com.iue.programacionweb.jpa.model.Pais;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CrudMapper {

  @Mapping(source = "id", target = "id")
  @Mapping(source = "aficcion", target = "aficion")
  AficionDto entity2dto(Aficcion aficcion);

  @Mapping(source = "id", target = "id")
  @Mapping(source = "contextura", target = "contextura")
  ContexturaDto entity2dto(Contextura contextura);

  @Mapping(source = "id", target = "id")
  @Mapping(source = "educacion", target = "educacion")
  EducacionDto entity2dto(Educacion educacion);

  @Mapping(source = "id", target = "id")
  @Mapping(source = "lugar", target = "lugar")
  LugarDto entity2dto(Lugar lugar);

  @Mapping(source = "id", target = "id")
  @Mapping(source = "pais", target = "pais")
  PaisDto entity2dto(Pais pais);
}
