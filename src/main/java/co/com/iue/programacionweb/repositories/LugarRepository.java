package co.com.iue.programacionweb.repositories;

import co.com.iue.programacionweb.jpa.model.Lugar;
import co.com.iue.programacionweb.jpa.model.Pais;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface LugarRepository
    extends JpaRepository<Lugar, Integer>, QuerydslPredicateExecutor<Lugar> {

  List<Lugar> findAllByPais(Pais pais);
}
