package co.com.iue.programacionweb.repositories;

import co.com.iue.programacionweb.jpa.model.Contextura;
import co.com.iue.programacionweb.jpa.model.Educacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ContexturaRepository extends JpaRepository<Contextura, Integer>, QuerydslPredicateExecutor<Contextura> {

}
