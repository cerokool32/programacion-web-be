package co.com.iue.programacionweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgramacionWebApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProgramacionWebApiApplication.class, args);
    }

}
