package co.com.iue.programacionweb.controller.dto;

import lombok.Data;

@Data
public class ContexturaDto {

  private Integer id;

  private String contextura;
}
