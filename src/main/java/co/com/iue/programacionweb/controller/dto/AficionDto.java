package co.com.iue.programacionweb.controller.dto;

import lombok.Data;

@Data
public class AficionDto {

  private Integer id;

  private String aficion;
}
